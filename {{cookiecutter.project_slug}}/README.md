# {{ cookiecutter.project_name }}



{{ cookiecutter.project_short_description}}


This repository can be used both as a python package and as a microservice.


1. *python package*  - a normal package hosted in the package repository.
2. *microservice* - package is installed inside a docker container.

## Quick start

1. build dev image with `build_dev_mage.sh`
2. open in VSCode devcontainer, develop.
3. use `build_and_run.sh` to build and run image locally
4. trigger ci builds by bumping version with a tag. (see `.gitlab-ci.yml`)

## Tooling

* Verisoning : `bump2version`
* Linting and formatting : `ruff`
* Typechecking: `mypy`

## What goes where
* `src/{{ cookiecutter.package_name }}` app code. `pip install .` .
* `docker` folder contains dockerfiles for images.
* `.gitlab-ci.yml` takes care of the building steps.
